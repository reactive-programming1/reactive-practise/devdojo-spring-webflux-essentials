package net.celloscope.devdojowebflux;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DevdojoWebfluxApplication {

	public static void main(String[] args) {
		SpringApplication.run(DevdojoWebfluxApplication.class, args);
	}

}
