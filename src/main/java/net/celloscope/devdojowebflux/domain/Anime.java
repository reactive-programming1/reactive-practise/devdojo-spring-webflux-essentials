package net.celloscope.devdojowebflux.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Table("Anime")
public class Anime {

    @Id
    Integer id;

    @NotNull
    @NotEmpty(message = "This field can't be empty")
    String name;
}
