package net.celloscope.devdojowebflux.service;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.celloscope.devdojowebflux.domain.Anime;
import net.celloscope.devdojowebflux.repository.AnimeRepository;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.annotation.Nullable;

import java.security.PublicKey;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;

@Service
@Slf4j
@RequiredArgsConstructor
public class AnimeService {
    private final AnimeRepository animeRepository;

    public Flux<Anime> getAllAnime() {
        return animeRepository.findAll();
    }


    public Mono<Anime> findByID(Integer id) {
        return animeRepository.findById(id)
                .switchIfEmpty(monoStatusNotFoundException(""))
                .doOnError(e -> e.printStackTrace())
                .log();
    }


    public <T> Mono<T> monoStatusNotFoundException(String value) {
        return Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND, value + " Not Found!"));
    }

    public Mono<Anime> save(Anime anime) {
        return animeRepository.save(anime);
    }

    public Mono<Void> update(Integer id,Anime anime) {
        return findByID(id)
                .map(anime1 -> {
                    anime1.setName(anime.getName());
                    return anime1;
                })
                .flatMap(animeRepository::save)
                .then();
    }

    public Mono<Void> delete(Integer id) {
        return findByID(id)
                .flatMap(animeRepository::delete)
                .then();
    }

    public Flux<Anime> saveAll(List<Anime> anime) {
        return animeRepository.saveAll(anime);
    }
}
