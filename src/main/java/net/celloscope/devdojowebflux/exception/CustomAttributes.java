//package net.celloscope.devdojowebflux.exception;
//
//import org.springframework.boot.web.error.ErrorAttributeOptions;
//import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
//import org.springframework.core.annotation.Order;
//import org.springframework.stereotype.Component;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.context.request.WebRequest;
//import org.springframework.web.server.ResponseStatusException;
//
//import java.util.Map;
//
//@Component
//@Order(-1)
//public class CustomAttributes extends DefaultErrorAttributes{
//    @Override
//    public Map<String, Object> getErrorAttributes(WebRequest webRequest, ErrorAttributeOptions options) {
//        Map<String, Object> errorAttributesMap =  super.getErrorAttributes(webRequest, options);
//        Throwable throwable = getError(webRequest);
//        if (throwable instanceof ResponseStatusException){
//            ResponseStatusException responseStatusException = (ResponseStatusException) throwable;
//            errorAttributesMap.put("message",responseStatusException.getMessage());
//            errorAttributesMap.put("message from SAJAL","Custom Exception throwing from MYSELF");
//        }
//        return errorAttributesMap;
//    }
//}
