package net.celloscope.devdojowebflux.controller;


import io.netty.util.internal.StringUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.celloscope.devdojowebflux.domain.Anime;
import net.celloscope.devdojowebflux.service.AnimeService;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("animes")
@RequiredArgsConstructor
public class AnimeController {
    private final AnimeService animeService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Flux<Anime> listAll() {
        return animeService.getAllAnime();
    }

    @GetMapping(path = "{id}")
    @ResponseStatus(HttpStatus.OK)
    public Mono<Anime> findById(@PathVariable Integer id) {
        return animeService.findByID(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<Anime> save(@Valid @RequestBody Anime anime) {
        return animeService.save(anime);
    }

    @Transactional //it rollback the action if it has any wrong logic
    @PostMapping("batch")
    @ResponseStatus(HttpStatus.CREATED)
    public Flux<Anime> batchSave(@RequestBody List<Anime> anime) {
        return animeService.saveAll(anime)
                .doOnNext(this::throwExceptionIfAnyEntityIsEmptyOrNull);
    }

    private void throwExceptionIfAnyEntityIsEmptyOrNull(Anime anime) {
        if (StringUtil.isNullOrEmpty(anime.getName()))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No Name");
    }

    @PutMapping(path = "{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public Mono<Void> update(@PathVariable Integer id, @Valid @RequestBody Anime anime) {
        return animeService.update(id, anime);
    }

    @DeleteMapping(path = "{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public Mono<Void> delete(@PathVariable Integer id) {
        return animeService.delete(id);
    }

}
