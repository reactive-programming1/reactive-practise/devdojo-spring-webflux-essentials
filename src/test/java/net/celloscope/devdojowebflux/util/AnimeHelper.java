package net.celloscope.devdojowebflux.util;

import net.celloscope.devdojowebflux.domain.Anime;

import java.util.List;

public class AnimeHelper {

    public static Anime createNewAnimeToBeSaved() {
        return Anime.builder()
                .name("Pufi Test")
                .build();
    }

    public static Anime createValidAnime() {
        return Anime.builder()
                .id(2)
                .name("Pufi Test")
                .build();
    }

    public static Anime createValidUpdatedAnime() {
        return Anime.builder()
                .id(1)
                .name("Pufi Test 2")
                .build();
    }

    public static Anime createInValidAnime() {
        return Anime.builder()
                .id(8)
                .name(null)
                .build();
    }

    public static List<Anime> validListOfAnimeObject() {
        return List.of(Anime.builder()
                        .name("Gfde")
                        .build(),
                Anime.builder()
                        .name("Sbxz")
                        .build(),
                Anime.builder()
                        .name("Xovm")
                        .build());
    }
}
