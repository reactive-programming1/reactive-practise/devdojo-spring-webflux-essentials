package net.celloscope.devdojowebflux.service;

import net.celloscope.devdojowebflux.domain.Anime;
import net.celloscope.devdojowebflux.repository.AnimeRepository;
import net.celloscope.devdojowebflux.util.AnimeHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
class AnimeServiceTest {

    @InjectMocks
    private AnimeService animeService;
    @Mock
    private AnimeRepository animeRepository;

    @BeforeEach
    public void setUp() {
    }

    @Test
    void shouldReturnFluxOfAnime() {

        //given
        Anime newAnimeFlux = AnimeHelper.createValidAnime();
        //when
        Mockito.when(animeRepository.findAll())
                .thenReturn(Flux.just(newAnimeFlux));
        //then
        StepVerifier.create(animeService.getAllAnime())
                .expectSubscription()
                .expectNext(newAnimeFlux)
                .verifyComplete();
    }


    @Test
    @DisplayName("FindById")
    void shouldReturnMonoOfAnimeByID() {
        //given
        Anime newAnimeMono = AnimeHelper.createValidAnime();
        //when
        Mockito.when(animeRepository.findById(ArgumentMatchers.anyInt()))
                .thenReturn(Mono.just(newAnimeMono));
        //then
        StepVerifier.create(animeService.findByID(4))
                .expectSubscription()
                .expectNext(newAnimeMono)
                .verifyComplete();
    }

    @Test
    @DisplayName("FindById NotFound")
    void shouldReturnAnExceptionWhenNotFoundById() {
        //given
        Anime newAnimeMono = AnimeHelper.createValidAnime();
        //when
        Mockito.when(animeRepository.findById(ArgumentMatchers.anyInt()))
                .thenReturn(Mono.empty());
        //then
        StepVerifier.create(animeService.findByID(4))
                .expectSubscription()
                .expectError(ResponseStatusException.class)
                .verify();
    }


    @Test
    @DisplayName("Save Operation")
    void givenAnime_whenSaveIsCalled_thenReturntheAnimeObject() {
        //given
        Anime saveThisAnime = AnimeHelper.createNewAnimeToBeSaved();
        //when
        Mockito.when(animeRepository.save(ArgumentMatchers.any(Anime.class)))
                .thenReturn(Mono.just(saveThisAnime));
        //then
        StepVerifier.create(animeService.save(saveThisAnime))
                .expectSubscription()
                .expectNext(saveThisAnime)
                .verifyComplete();
    }


    @Test
    @DisplayName("Batch Save Operation")
    void givenListOfAnime_whenBatchSaveIsCalled_thenReturntheListOfAnimeObject() {
        //given
        List<Anime> saveThisAnimeList = AnimeHelper.validListOfAnimeObject();
        //when
        Mockito.when(animeRepository.saveAll(ArgumentMatchers.anyList()))
                .thenReturn(Flux.fromIterable(saveThisAnimeList));
        //then
        StepVerifier.create(animeService.saveAll(saveThisAnimeList))
                .expectSubscription()
                .assertNext(
                        t -> assertThat(t).isEqualTo(saveThisAnimeList.get(0))
                )
                ;
    }

    @Test
    @DisplayName("Batch Save Operation Test 2")
    void givenListOfAnime_whenBatchSaveIsCalled_thenReturnTheListOfAnimeObject_Test2() {
        //given
        List<Anime> saveThisAnimeList = AnimeHelper.validListOfAnimeObject();
        //when
        Mockito.when(animeRepository.saveAll(ArgumentMatchers.anyList()))
                .thenReturn(Flux.fromIterable(saveThisAnimeList));
        //then
        StepVerifier.create(animeService.saveAll(saveThisAnimeList))
                .expectSubscription()
                .expectNextSequence(saveThisAnimeList)
                .verifyComplete();
        ;
    }

    @Test
    @DisplayName("Delete Test")
    void givenAnimeId_whenDeleteIsCalled_thenReturnNothingIfItIsSuccessful() {
        //given
        int id = 2;
        Anime toBeDeletedThis = AnimeHelper.createValidAnime();
        //when
        Mockito.when(animeRepository.findById(ArgumentMatchers.anyInt()))
                .thenReturn(Mono.just(toBeDeletedThis));
        Mockito.when(animeRepository.delete(ArgumentMatchers.any(Anime.class)))
                .thenReturn(Mono.empty());
        //then
        StepVerifier.create(animeService.delete(id))
                .expectSubscription()
                .verifyComplete();
    }

    @Test
    @DisplayName("Delete Error Test")
    void givenAnimeId_whenDeleteIsCalled_thenReturnError() {
        //given
        int id = 2;
        //when
        Mockito.when(animeRepository.findById(ArgumentMatchers.anyInt()))
                .thenReturn(Mono.empty());
        //then
        StepVerifier.create(animeService.delete(id))
                .expectError(ResponseStatusException.class)
                .verify();
    }


    @Test
    @DisplayName("Update Test")
    void givenAnime_whenUpdateIsCalled_thenValueShouldBeUpdated() {
        //given
        Anime updateThisAnime = AnimeHelper.createValidUpdatedAnime();
        //when
        Mockito.when(animeRepository.findById(ArgumentMatchers.anyInt()))
                .thenReturn(Mono.just(updateThisAnime));
        Mockito.when(animeRepository.save(updateThisAnime))
                .thenReturn(Mono.empty());
        //then
        StepVerifier.create(animeService.update(2, updateThisAnime))
                .expectSubscription()
                .verifyComplete();
    }


    @Test
    @DisplayName("Update Error Test")
    void givenAnime_whenUpdateIsCalled_thenValueShouldNotUpdate_willGiveAnError() {
        //given
        Anime updateThisAnime = AnimeHelper.createValidUpdatedAnime();
        //when
        Mockito.when(animeRepository.findById(ArgumentMatchers.anyInt()))
                .thenReturn(Mono.empty());
        //then
        StepVerifier.create(animeService.update(5, updateThisAnime))
                .expectSubscription()
                .expectError(ResponseStatusException.class)
                .verify();
    }


}