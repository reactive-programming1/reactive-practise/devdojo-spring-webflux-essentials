package net.celloscope.devdojowebflux.integration;

import lombok.extern.slf4j.Slf4j;
import net.celloscope.devdojowebflux.domain.Anime;
import net.celloscope.devdojowebflux.repository.AnimeRepository;
import net.celloscope.devdojowebflux.service.AnimeService;
import net.celloscope.devdojowebflux.util.AnimeHelper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.autoconfigure.webservices.client.WebServiceClientTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.List;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient
@Slf4j
public class AnimeControllerIntegrationTest {

    @Autowired
    private WebTestClient webTestClient;
    @MockBean
    private AnimeRepository animeRepository;


    @Test
    @DisplayName("View Full List Test")
    void givenNothing_whenCallingListAllMethod_thenReturnTheFullList() {
        webTestClient
                .get()
                .uri("/animes")
                .exchange()
                .expectStatus()
                .is2xxSuccessful();
    }


    @Test
    @DisplayName("View Full List with details Test")
    void givenDataTypesExpectations_whenCallingListAllMethod_thenReturnTheFullList() {
        //given
        Anime buildAnime = AnimeHelper.createValidAnime();
        //when
        Mockito.when(animeRepository.findAll())
                .thenReturn(Flux.just(buildAnime));
        //then

        webTestClient
                .get()
                .uri("/animes")
                .exchange()
                .expectStatus()
                .is2xxSuccessful()
                .expectBody()
                .jsonPath("$.[0].id").isEqualTo(buildAnime.getId())
                .jsonPath("$.[0].name").isEqualTo(buildAnime.getName())
        ;
    }


    @Test
    @DisplayName("View Full List with details Test No JSON body")
    void givenData_whenCallingListAllMethod_thenReturnTheFullList_InAListFormat() {
        //given
        Anime buildAnime = AnimeHelper.createValidAnime();
        //when
        Mockito.when(animeRepository.findAll())
                .thenReturn(Flux.just(buildAnime));
        //then
        webTestClient
                .get()
                .uri("/animes")
                .exchange()
                .expectStatus()
                .is2xxSuccessful()
                .expectBodyList(Anime.class)
                .hasSize(1)
                .contains(buildAnime)
        ;
    }

    @Test
    @DisplayName("FindBy User")
    void givenAnimeId_whenFindByIdIsCalled_thenReturnTheIdInformation() {
        //given
        int id = 25;
        Anime myAnime = AnimeHelper.createValidAnime();
        //when
        Mockito.when(animeRepository.findById(ArgumentMatchers.anyInt()))
                .thenReturn(Mono.just(myAnime));
        //then
        webTestClient
                .get()
                .uri("/animes/{id}", id)
                .exchange()
                .expectStatus()
                .is2xxSuccessful()
                .expectBody(Anime.class)
                .isEqualTo(myAnime);
    }


    @Test
    @DisplayName("FindBy User Not Found")
    void givenAnimeId_whenFindByIdIsCalled_thenReturnErrorAsItIsNotFound() {
        //given
        int id = 25;
        Anime myAnime = AnimeHelper.createValidAnime();
        //when
        Mockito.when(animeRepository.findById(ArgumentMatchers.anyInt()))
                .thenReturn(Mono.empty());
        //then
        webTestClient
                .get()
                .uri("/animes/{id}", id)
                .exchange()
                .expectStatus()
                .isNotFound()
        ;
    }


    @Test
    @DisplayName("FindBy User Not Found with Error Body")
    void givenAnimeId_whenFindByIdIsCalled_thenReturnErrorAsItIsNotFoundWithErrorMatchingBody() {
        //given
        int id = 25;
        Anime myAnime = AnimeHelper.createValidAnime();
        //when
        Mockito.when(animeRepository.findById(ArgumentMatchers.anyInt()))
                .thenReturn(Mono.empty());
        //then
        webTestClient
                .get()
                .uri("/animes/{id}", id)
                .exchange()
                .expectStatus()
                .isNotFound()
                .expectBody()
                .jsonPath("$.status").isEqualTo("404")
                .jsonPath("$.error").isEqualTo("Not Found")
        ;
    }

    @Test
    @DisplayName("Save Object Test")
    void givenAnimeObject_whenFindByIdIsCalled_thenReturnErrorAsItIsNotFoundWithErrorMatchingBody() {
        //given
        int id = 25;
        Anime myAnime = AnimeHelper.createValidAnime();
        //when
        Mockito.when(animeRepository.findById(ArgumentMatchers.anyInt()))
                .thenReturn(Mono.empty());
        //then
        webTestClient
                .get()
                .uri("/animes/{id}", id)
                .exchange()
                .expectStatus()
                .isNotFound()
                .expectBody()
                .jsonPath("$.status").isEqualTo("404")
                .jsonPath("$.error").isEqualTo("Not Found")
        ;
    }

    @Test
    @DisplayName("Save object Test")
    void givenAnimeObject_whenSaveExecuted_thenReturnSavedObject() {
        //given
        Anime myAnime = AnimeHelper.createValidAnime();
        //when
        Mockito.when(animeRepository.save(ArgumentMatchers.any(Anime.class)))
                .thenReturn(Mono.just(myAnime));
        //then
        webTestClient
                .post()
                .uri("/animes")
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(myAnime))
                .exchange()
                .expectStatus().isCreated()
                .expectBody(Anime.class)
                .isEqualTo(myAnime)
        ;
    }


    @Test
    @DisplayName("Batch Save All objects Test")
    void givenListOfAnimeObject_whenBatchSaveExecuted_thenReturnListOfSavedObject() {
        //given
        List<Anime> saveThisAnimeList = AnimeHelper.validListOfAnimeObject();
        //when
        Mockito.when(animeRepository.saveAll(ArgumentMatchers.anyList()))
                .thenReturn(Flux.fromIterable(saveThisAnimeList));
        //then
        webTestClient
                .post()
                .uri("/animes/batch")
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(saveThisAnimeList))
                .exchange()
                .expectStatus().isCreated()
                .expectBodyList(Anime.class)
                ;
    }

    @Test
    @DisplayName("Save fails when object is invalid Test")
    void givenAnimeObjectWithInappropriateValue_whenSaveExecuted_thenReturnError() {
        //given
        Anime nullAnime = AnimeHelper.createInValidAnime();
        //when
        Mockito.when(animeRepository.save(ArgumentMatchers.any(Anime.class)))
                .thenReturn(Mono.error(new RuntimeException()));
        //then
        webTestClient
                .post()
                .uri("/animes")
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(nullAnime))
                .exchange()
                .expectStatus().isBadRequest()
                .expectBody()
                .jsonPath("$.status").isEqualTo("400")
        ;
    }

    @Test
    @DisplayName("Delete object Test")
    void givenAnimeObjectId_whenDeleteCalls_thenExecuteSuccessfully() {
        //given
        int id = 2;
        Anime delAnime = AnimeHelper.createValidAnime();
        //when
        Mockito.when(animeRepository.findById(ArgumentMatchers.anyInt()))
                .thenReturn(Mono.just(delAnime));
        Mockito.when(animeRepository.delete(ArgumentMatchers.any(Anime.class)))
                .thenReturn(Mono.empty());
        //then
        webTestClient
                .delete()
                .uri("/animes/{id}", id)
                .exchange()
                .expectStatus().isNoContent()
        ;
    }


    @Test
    @DisplayName("Delete Invalid object Not Found Test")
    void givenInvalidAnimeObjectId_whenDeleteCalls_thenGetsError() {
        //given
        int id = 2;
        Anime delAnime = AnimeHelper.createValidAnime();
        //when
        Mockito.when(animeRepository.findById(ArgumentMatchers.anyInt()))
                .thenReturn(Mono.empty());
        //then
        webTestClient
                .delete()
                .uri("/animes/{id}", id)
                .exchange()
                .expectStatus().isNotFound()
        ;
    }

    @Test
    @DisplayName("Update object Test")
    void givenAnimeObject_whenUpdateExecuted_thenReturnSavedObject() {
        //given
        int id = 3;
        Anime myAnime = AnimeHelper.createValidAnime();
        //when
        Mockito.when(animeRepository.findById(ArgumentMatchers.anyInt()))
                .thenReturn(Mono.just(myAnime));
        Mockito.when(animeRepository.save(ArgumentMatchers.any(Anime.class)))
                .thenReturn(Mono.empty());
        //then
        webTestClient
                .put()
                .uri("/animes/{id}", id)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(myAnime))
                .exchange()
                .expectStatus().isNoContent()
        ;
    }


    @Test
    @DisplayName("Update object Failed Test")
    void givenInvalidAnimeObjectId_whenUpdateExecuted_thenReturnError() {
        //given
        int id = 3;
        Anime myAnime = AnimeHelper.createValidAnime();
        //when
        Mockito.when(animeRepository.findById(ArgumentMatchers.anyInt()))
                .thenReturn(Mono.empty());
        //then
        webTestClient
                .put()
                .uri("/animes/{id}", id)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(myAnime))
                .exchange()
                .expectStatus().isNotFound()
        ;
    }

}
